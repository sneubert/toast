#
# Copyright 2019 CERN for the benefit of the LHCb collaboration
#
# Licensed under the LCPGL License, Version 3 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License here:
#
#     https://opensource.org/licenses/LGPL-3.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IT IS" basis,
# without warranties or conditions of any kind, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Author: Alessio Piucci, alessio.piucci@cern.ch
#         (based on previous software by Adam Morris)
# ==============================================================================

import tensorflow as tf
import sys
import numpy as np
import math

#import the local libraries
import Interface
from Interface import fptype, Cos, Acos, Const, Atan2

import Kinematics

from Constants import *

# import the logging configuration
import logging
#from logging.config import fileConfig
#
#logging.config.fileConfig('../include/logging.ini')

######

sess = None

######

class ThreeBodyPhaseSpace :
	"""
	Class for 3-body decay phase space M -> d1 d2 d3 expressed as:
	  cosThetaM   : cos(thetaM), cosine of the helicity angle of M
	  cosThetaA   : cos(thetaA), cosine of the helicity angle of A
          phid1       : angle between the M and A planes in the M rest frame
        Remember that phid3 == 0 by convention!
	"""

        def __init__(self) :
                """
                  Constructor                                                                                                       
                """

                # set the masses (as floats, not tensors)
		self.m_M = M.mass_float
		self.m_d1 = d1.mass_float
		self.m_d2 = d2.mass_float
		self.m_d3 = d3.mass_float

                self.m_A_min = self.m_d1 + self.m_d2
		self.m_A_max = self.m_M - self.m_d3
                                
                self.m_B_min = self.m_d2 + self.m_d3
                self.m_B_max = self.m_M - self.m_d1
                
                self.m_C_min = self.m_d1 + self.m_d3
                self.m_C_max = self.m_M - self.m_d2
                                
		self.data_placeholder = self.Placeholder("data")
		self.norm_placeholder = self.Placeholder("data")

        def Inside(self, x) :
                """
                  Check if the point x is inside the phase space
                """
                
                m_d1d2 = self.m_d1d2(x)
                m_d2d3 = self.m_d2d3(x)
                m_d1d3 = self.m_d1d3(x)
                
                # helicity angles
       		cthM = self.cosThetaM(x)
		cthA = self.cosThetaA(x)
		
                # phid1: phi angle between the M and A planes in the M rest frame 
                phid1 = self.phid1(x)
                
                # remember that phid3 == 0!
                
                # check if the cosin and phi angles are in the phase space
                inside = tf.logical_and(tf.logical_and(tf.logical_and(tf.greater(cthM, -1.), tf.less(cthM, 1.)), \
		                                       tf.logical_and(tf.greater(cthA, -1.), tf.less(cthA, 1.))), \
                                        tf.logical_and(tf.greater(phid1, -3.14), tf.less(phid1, 3.14)))
                
                # check if the invariant masses are in the phase space
                inside = tf.logical_and(inside,
                                        tf.logical_and(tf.logical_and(tf.greater(m_d1d2, self.m_A_min), tf.less(m_d1d2, self.m_A_max)),
                                                       tf.logical_and(tf.logical_and(tf.greater(m_d2d3, self.m_B_min), tf.less(m_d2d3, self.m_B_max)),
                                                                      tf.logical_and(tf.greater(m_d1d3, self.m_C_min), tf.less(m_d1d3, self.m_C_max)))))
                
		return inside
        
	def Filter(self, x) :
                """
                  Filter a sample, checking that it is inside the allowed phase space
                """
                
		return tf.boolean_mask(x, self.Inside(x) )

        def Density(self, x) :
                """
                  Get the density, playing with tensors
                """
	        
		m_A = self.m_d1d2(x)

                #ones = Ones(m_A)        # I would need it only in case of some more dn contributions without m_A inside, like:
                #                        d3 = TwoBodyMomentum(self.m_d2, self.m_pi, self.m_d3*ones)

                d1 = Kinematics.TwoBodyMomentum(self.m_M, m_A, self.m_d3)    # M --> A d3
		d2 = Kinematics.TwoBodyMomentum(m_A, self.m_d2, self.m_d1)   # A --> d1 d2
		
		return d1*d2/self.m_M

        def Density_NotTensor(self, x) :
                """
                  Get the density
                """

                # here the A mass is floating!
                
		d1 = Kinematics.TwoBodyMomentum_NotTensor(self.m_M, x, self.m_d3)    # M --> A d3
		d2 = Kinematics.TwoBodyMomentum_NotTensor(x, self.m_d2, self.m_d1)   # A --> d1 d2
                
                return d1*d2/self.m_M

	def m_d1d2Sample(self, size) :

                """
                  Generate a sample uniform in the d1d2 mass distribution
                """
                
		sample = []
                
                ymin = 0.
		ymax = self.Density_NotTensor(np.random.uniform(self.m_A_min, self.m_A_max, 10000)).max()

                while len(sample) < size:
			x = np.random.uniform(self.m_A_min, self.m_A_max)
			y = np.random.uniform(ymin, ymax)
                                        
			if y < self.Density_NotTensor(x):
				sample.append(x)

		return np.array(sample).astype('d')

        def UnfilteredSample(self, size, majorant = -1) :
	        """
	          Generate a uniform sample of point within phase space.
	          size     : number of _initial_ points to generate. Not all of them will fall into phase space,
	                     so the number of points in the output will be < size.
	          majorant : if majorant>0, add another dimension to the generated tensor which is
	                     uniform number from 0 to majorant. Useful for accept-reject toy MC.
                """
        
	        v = [
                        # m_d1d2
			self.m_d1d2Sample(size),
                        
                        # cos(thetaM_A)
                        np.random.uniform(-1., 1., size ).astype('d'),

                        # cos(thetaA)
                        np.random.uniform(-1., 1., size ).astype('d'),

                        # phid1_A
                        np.random.uniform(-math.pi, math.pi, size ).astype('d'),
                ]

                if majorant > 0 :
                        v += [ np.random.uniform( 0., majorant, size).astype('d') ]

		return np.transpose(np.array(v))

	def UniformSample(self, size, majorant = -1) :
		"""
		  Generate a uniform sample of point within phase space.
		    size     : number of _initial_ points to generate. Not all of them will fall into phase space,
		               so the number of points in the output will be <size.
		    majorant : if majorant>0, add another dimension to the generated tensor which is
		               uniform number from 0 to majorant. Useful for accept-reject toy MC.
		  Note it does not actually generate the sample, but returns the data flow graph for generation,
		  which has to be run within TF session.
		"""
		return self.Filter( self.UnfilteredSample(size, majorant) )

        ##########
        
        # basic variables to describe the amplitudes

        def m_d1d2(self, sample) :
        	return tf.transpose(sample)[0]

        def m2_d1d2(self, sample) :
                return tf.transpose(sample)[0]**2
        
        def cosThetaM(self, sample) :
                return tf.transpose(sample)[1]
                                        
	def cosThetaA(self, sample) :
		return tf.transpose(sample)[2]

	def phid1(self, sample) :
		return tf.transpose(sample)[3]

        ##########

        # some other interesting variables:
        # invariant masses of the subsystems, other angles
        
        def m_d2d3(self, sample) :
                """
                  Compute the invariant mass of the d2d3 subsystem
                """
                
                qvect_d1, qvect_d2, qvect_d3 = self.FinalStateMomenta(x = sample)

                return Kinematics.Mass(Kinematics.SumFourVectors([qvect_d2, qvect_d3]))

        def m_d1d3(self, sample) :
                """
                  Compute the invariant mass of the d1d3 subsystem
                """

                qvect_d1, qvect_d2, qvect_d3 = self.FinalStateMomenta(x = sample)
                
                return Kinematics.Mass(Kinematics.SumFourVectors([qvect_d1, qvect_d3]))

        def m2_d1d3(self, sample) :
                
                qvect_d1, qvect_d2, qvect_d3 = self.FinalStateMomenta(x = sample)
                
                return Kinematics.Mass(Kinematics.SumFourVectors([qvect_d1, qvect_d3]))**2
        
        def cosThetaB(self, sample) :
                """
                  Compute cos(thetaB)
                """
                
                theta_B = self.BCoordinates(sample)["theta_B"]
                
                return Cos(theta_B)

        def cosThetaM_B(self, sample) :
                
                theta_M_B = self.BCoordinates(sample)["theta_M_B"]

                return Cos(theta_M_B)
        
        def m2_d2d3(self, sample) :
                
                m2_d2d3 = self.BCoordinates(sample)["m2_d2d3"]

                return m2_d2d3
        
        def phid1_B(self, sample) :
                """
                  Compute phi_d1 in the B coordinate system
                """
                
                phi_d1_B = self.BCoordinates(sample)["phi_d1_B"]
                
                return phi_d1_B

        def phid2_B(self, sample) :

                phi_d2_B = self.BCoordinates(sample)["phi_d2_B"]

                return phi_d2_B
        
        def cosThetaC(self, sample) :
                """
                  Compute cos(thetaC)
                """
                
                theta_C = self.CCoordinates(sample)["theta_C"]
                        
                return Cos(theta_C)

        def cosThetaM_C(self, sample) :
                
                theta_M_C = self.CCoordinates(sample)["theta_M_C"]

                return Cos(theta_M_C)

        def phid2_C(self, sample) :
                """
                  Compute phi_d2 in the C coordinate system
                """
                
                phi_d2_C = self.CCoordinates(sample)["phi_d2_C"]

                return phi_d2_C

        def phid1_C(self, sample) :
                """
                  Compute phi_d1 in the C coordinate system
                """
                
                phi_d1_C = self.CCoordinates(sample)["phi_d1_C"]

                return phi_d1_C
        
        ##########

        # some auxiliary functions
        # to compute the 4-moments of the particles
        # and the kinematic variables
        
        def M_qvect(self, sample) :
                """
                  Get the M 4-momenta
                """
                
                qvect_d1, qvect_d2, qvect_d3 = self.FinalStateMomenta(x = sample)

                #compute the M 4-momenta
                return Kinematics.SumFourVectors([qvect_d1, qvect_d2, qvect_d3])

        def d1_qvect(self, sample) :
                """
                  Get the d1 4-momenta 
                """

                qvect_d1, qvect_d2, qvect_d3 = self.FinalStateMomenta(x = sample)

                return qvect_d1

        def d2_qvect(self, sample) :
                """
                  Get the d2 4-momenta
                """

                qvect_d1, qvect_d2, qvect_d3 = self.FinalStateMomenta(x = sample)

                return qvect_d2

        def d3_qvect(self, sample) :
                """  
                  Get the d3 4-momenta
                """

                qvect_d1, qvect_d2, qvect_d3 = self.FinalStateMomenta(x = sample)

                return qvect_d3

        def M_PX(self, sample) :
                return Kinematics.XComponent(self.M_qvect(sample))

        def M_PY(self, sample) :
                return Kinematics.YComponent(self.M_qvect(sample))

        def M_PZ(self, sample) :
                return Kinematics.ZComponent(self.M_qvect(sample))

        def M_E(self, sample) :
                return Kinematics.TimeComponent(self.M_qvect(sample))

        def d1_PX(self, sample) :
                return Kinematics.XComponent(self.d1_qvect(sample))

        def d1_PY(self, sample) :
                return Kinematics.YComponent(self.d1_qvect(sample))

        def d1_PZ(self, sample) :
                return Kinematics.ZComponent(self.d1_qvect(sample))

        def d1_E(self, sample) :
                return Kinematics.TimeComponent(self.d1_qvect(sample))

        def d2_PX(self, sample) :
                return Kinematics.XComponent(self.d2_qvect(sample))

        def d2_PY(self, sample) :
                return Kinematics.YComponent(self.d2_qvect(sample))

        def d2_PZ(self, sample) :
                return Kinematics.ZComponent(self.d2_qvect(sample))

        def d2_E(self, sample) :
                return Kinematics.TimeComponent(self.d2_qvect(sample))

        def d3_PX(self, sample) :
                return Kinematics.XComponent(self.d3_qvect(sample))

        def d3_PY(self, sample) :
                return Kinematics.YComponent(self.d3_qvect(sample))

        def d3_PZ(self, sample) :
                return Kinematics.ZComponent(self.d3_qvect(sample))

        def d3_E(self, sample) :
                return Kinematics.TimeComponent(self.d3_qvect(sample))

        ##########
        
	def FinalStateMomenta(self, x,
                              m_M = None, m_d1 = None, m_d2 = None, m_d3 = None,
                              test = False) :
		"""
		  Returns the final state momenta for the decay defined by the phase space vector x.
		  The momenta are calculated in the Lambda_b frame.
		"""

                if m_M is None :
                        m_M = self.m_M

                if m_d1 is None :
                        m_d1 = self.m_d1
                        
                if m_d2 is None :
                        m_d2 = self.m_d2

                if m_d3 is None :
                        m_d3 = self.m_d3

                # if in test-mode, overwrite the "x" dataset with some toy events
                # and print a set of infos at the end of the transformations
                if test :
                        x = self.UnfilteredSample(2)
                        
                ones = Interface.Ones(self.m_d1d2(x))
		zeros = Interface.Zeros(self.m_d1d2(x))    #only used to set phid3 == 0 
                
                # get the A and d3 four-momenta in the M rest frame from the angles
                d3_qvect_Mrest, A_qvect_Mrest = Kinematics.FourMomentaFromHelicityAngles(ones*m_M, ones*m_d3, self.m_d1d2(x),
                                                                                         Acos(self.cosThetaM(x)), zeros)
                
                # get the d1 and d2 four-momenta in the A rest frame from the angles
                d2_qvect_Arest, d1_qvect_Arest = Kinematics.FourMomentaFromHelicityAngles(self.m_d1d2(x), ones*m_d2, ones*m_d1,
                                                                                          Acos(self.cosThetaA(x)), self.phid1(x))
                
                # boost the d1 and d2 four-momenta to the M rest frame,
                # boosting by -qvect_A (A is decaying to d1 and d2!)
                # Pay attention: this method is BoostAndRotation,
                # instead RotationAndBoost will be used in the following methods
                d1_qvect_Mrest, d2_qvect_Mrest = Kinematics.BoostAndRotation([d1_qvect_Arest, d2_qvect_Arest], A_qvect_Mrest)
                
	        return d1_qvect_Mrest, d2_qvect_Mrest, d3_qvect_Mrest

        def ACoordinates_fromqvects(self, d1_qvect, d2_qvect, d3_qvect):
                
                #########
                # m2 of d1d2, in GeV^2/c^4
                #########
                
                qvect_A = Kinematics.SumFourVectors([d1_qvect, d2_qvect])
                m2_d1d2 = (Kinematics.Mass(qvect_A))**2
                
                ##########
                # thetaM, in the M rest frame
                ##########
                
                # fill the M 4-momenta
                qvect_M = Kinematics.SumFourVectors([qvect_A + d3_qvect])
                
                # rotate and boost the d3 to the M rest frame
                d3_qvect_Mframe = Kinematics.RotationAndBoost([d3_qvect], qvect_M)[0]
                
                # calculate the angles:
	        # polar (theta) and azimuthal (phi) angles of the d3 three-vector in the M rest frame
                theta_M_A, phi_d3_A = Kinematics.HelicityAngles(d3_qvect_Mframe)
                
                # set the phi_d3_A angle = 0. by definition
                # well, I'm not returning it anyway...
                #phi_d3_A = Interface.Zeros(sample)
                
                #########
                # thetaA, in the A rest frame
                ##########
                
                # rotate and boost the d1 momenta to the rest frame of A
                d1_qvect_Aframe = Kinematics.RotationAndBoost([d1_qvect], qvect_A)[0]
                
                # calculate the angles:
                # polar (theta) and azimuthal (phi) angles of the d1 three-vector in the A rest frame
                theta_A, phi_d1_A = Kinematics.HelicityAngles(d1_qvect_Aframe)
                
                return m2_d1d2, theta_M_A, theta_A, phi_d1_A
                
        def ACoordinates(self, x):
                """
                  Returns the invariant mass and angles needed for the A decay chain,
                  as a dictionary
                """
                
                m2_d1d2 = self.m_d1d2(x)**2
                theta_M_A = Acos(self.cosThetaM(x))
		theta_A = Acos(self.cosThetaA(x))
                phi_d1_A = self.phid1(x)
                
                # build the dictionary
                dictionary = {"m2_d1d2"    : self.m_d1d2(x)**2,
                              "theta_M_A"  : Acos(self.cosThetaM(x)),
                              "theta_A"    : Acos(self.cosThetaA(x)),
                              "phi_d1_A"   : self.phid1(x)
                }
                
                return dictionary
        
	def BCoordinates(self, x,
                         d1_qvect = None, d2_qvect = None, d3_qvect = None,
                         test = False):
                
		"""
		  Returns the invariant mass and angles needed for the B decay chain,
                  as a dictionary
                """
                
                # do you want to use some externally-provided 4-momenta,
                # or compute the from the A coordinates?
                if d1_qvect is None or d2_qvect is None or d3_qvect is None :
                        
                        # compute the four-momenta, in the M decay frame
                        d1_qvect, d2_qvect, d3_qvect = self.FinalStateMomenta(x = x, test = test)
                else :
                        # use the 4-momenta, but boost them to the M rest frame
			M_qvect = Kinematics.SumFourVectors([d1_qvect, d2_qvect, d3_qvect])
			d1_qvect, d2_qvect, d3_qvect = Kinematics.RotationAndBoost([d1_qvect, d2_qvect, d3_qvect], M_qvect)
                
                # calculate the angles:
                # polar (theta) and azimuthal (phi) angles of the d1 three-vector in the M frame
                theta_M_B, phi_d1_B = Kinematics.HelicityAngles(d1_qvect)
                
                B_qvect = Kinematics.SumFourVectors([d2_qvect, d3_qvect])
                
                # rotate and boost the d2 momenta to the rest frame of the B
                d2_qvect_B = Kinematics.RotationAndBoost([d2_qvect], B_qvect)[0]  # it takes as input a list of 4-momenta to transform,
                                                                                  # it returns the list of transformed 4-momenta
                
                # calculate the angles:
                # polar (theta) and azimuthal (phi) angles of the d2 three-vector in the B rest frame
                theta_B, phi_d2_B = Kinematics.HelicityAngles(d2_qvect_B)
                
                #########
                # now I have to compute alpha_d1_AB and theta_d1_AB,
                # because of the different d1 helicity axes in the A and B decay chains
                #########        
                
                # boost the d2 and B to the d1 rest frame
                d2_qvect_d1Rest, B_qvect_d1Rest = Kinematics.RotationAndBoost([d2_qvect, B_qvect], d1_qvect)
                
                # get the three-momenta versors
                d2_p3vers = Kinematics.UnitVector(Kinematics.SpatialComponents(d2_qvect_d1Rest))
                B_p3vers = Kinematics.UnitVector(Kinematics.SpatialComponents(B_qvect_d1Rest))
                
                # compute theta_d1_AB
                theta_d1_AB = Acos(Kinematics.ScalarProduct(d2_p3vers, B_p3vers))
                
                #####
                # aplha_d1_AB is == 0. by 'construction'
                #####
                
                # build the dictionary
		dictionary = {"m2_d2d3"     : Kinematics.Mass(Kinematics.SumFourVectors([d2_qvect, d3_qvect]))**2,
                              "theta_M_B"   : theta_M_B,
                              "theta_B"     : theta_B,
                              "phi_d1_B"    : phi_d1_B,
                              "phi_d2_B"    : phi_d2_B,
                              "theta_d1_AB" : theta_d1_AB
                }
                
		# return the coordinates
		return dictionary

        def CCoordinates(self, x,
                         d1_qvect = None, d2_qvect = None, d3_qvect = None,
                         test = False):
                
                """
                  Returns the invariant mass and angles needed for the C decay chain,
                  as a dictionary
                """

                # do you want to use some externally-provided 4-momenta,
                # or compute the from the A coordinates?
		if d1_qvect is None or d2_qvect is None or d3_qvect is None :
                        # compute the four-momenta, in the M decay frame
		        d1_qvect, d2_qvect, d3_qvect = self.FinalStateMomenta(x = x, test = test)
		else :
                        # use the 4-momenta, but boost them to the M rest frame
                        M_qvect = Kinematics.SumFourVectors([d1_qvect, d2_qvect, d3_qvect])
                        d1_qvect, d2_qvect, d3_qvect = Kinematics.RotationAndBoost([d1_qvect, d2_qvect, d3_qvect], M_qvect)
                        
                # calculate the angles
                # polar (theta) and azimuthal (phi) angles of the d2 three-vector in the M frame
                theta_M_C, phi_d2_C = Kinematics.HelicityAngles(d2_qvect)

                C_qvect = Kinematics.SumFourVectors([d1_qvect, d3_qvect])
                
                # rotate and boost the C momenta to the rest frame of the d2
                #qvect_C_d2 = Kinematics.RotationAndBoost([qvect_C], qvect_d2)[0]  # Probably have to rotate by some phi angle
                #
                # calculate the angles
                #theta_C, phi_d1_C = Kinematics.HelicityAngles(qvect_C_d2)
                
                # rotate and boost the d1 momenta to the rest frame of C
                d1_qvect_C = Kinematics.RotationAndBoost([d1_qvect], C_qvect)[0]  # it takes as input a list of 4-momenta to transform,
                      	      	      	      	      	      	      	          # it returns the list of transformed 4-momenta
                                                                                  
                theta_C, phi_d1_C = Kinematics.HelicityAngles(d1_qvect_C)
                
                #########
                # now I have to compute alpha_d1_AB and theta_d1_AB,
                # because of the different d1 helicity axes in the A and B decay chains
                #########

                """
                theta_d1_AC = Const(0.)
                return (Kinematics.Mass(Kinematics.SumFourVectors([qvect_d1, qvect_d3]))**2,
                        theta_M_C, theta_C, phi_d2_C, phi_d1_C, theta_d1_AC)
                """
        
                # boost the d2 and C to the d1 rest frame
                d2_qvect_d1Rest, C_qvect_d1Rest = Kinematics.RotationAndBoost([d2_qvect, C_qvect], d1_qvect)
                
                # get the three-momenta versors
                d2_p3vers = Kinematics.UnitVector(Kinematics.SpatialComponents(d2_qvect_d1Rest))
                C_p3vers = Kinematics.UnitVector(Kinematics.SpatialComponents(C_qvect_d1Rest))
                
                # compute theta_d1_AC
                theta_d1_AC = Acos(Kinematics.ScalarProduct(d2_p3vers, C_p3vers))
                
                #####
                # aplha_d1_AC is == 0. by 'construction'
                #####

                # build the dictionary
	        dictionary = {"m2_d1d3"     : Kinematics.Mass(Kinematics.SumFourVectors([d1_qvect, d3_qvect]))**2,
                              "theta_M_C"   : theta_M_C,
                              "theta_C"     : theta_C,
                              "phi_d2_C"    : phi_d2_C,
                              "phi_d1_C"    : phi_d1_C,
                              "theta_d1_AC" : theta_d1_AC
                }
                
                # return the coordinates
                return dictionary
                
	def Placeholder(self, name = None) :
		return tf.placeholder(fptype, shape = (None, None), name = name )

