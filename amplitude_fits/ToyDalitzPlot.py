#
# Copyright 2019 CERN for the benefit of the LHCb collaboration
#
# Licensed under the LCPGL License, Version 3 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License here:
#
#     https://opensource.org/licenses/LGPL-3.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IT IS" basis,
# without warranties or conditions of any kind, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Author: Alessio Piucci, alessio.piucci@cern.ch
# ==============================================================================

import sys
import argparse

import tensorflow as tf

from ROOT import TFile, TTree, TH2F, TCanvas, gStyle

#import the local libraries
import PhaseSpace
import Fit
import Plot

import Optimisation

from Constants import *
from variables_Lb2LcD0K import variables, variables_kin, other_variables

if __name__ == "__main__" :

        ###########
        ## parser
        ###########
      
        parser = argparse.ArgumentParser(description="Parser of the ToyDalitzPlot script")
        
        #get the input file and tree names
        parser.add_argument('--inFile_name', dest='inFile_name',
                            default='toy.root', help="name of the input toy MC file")
        parser.add_argument('--inTree_name', dest='inTree_name',
                            default='toy', help="name of the input toy MC tree")
        
        #output file name
        parser.add_argument('--outFile_name', dest='outFile_name',
                            default='DalitzToy', help="output file name")
        
        #now get the parsed options
        args = parser.parse_args()
        
        inFile_name = args.inFile_name
        inTree_name = args.inTree_name
        outFile_name = args.outFile_name
        
        #print the parsed options
        print "\n"
        print "inFile_name = ", inFile_name
        print "inTree_name = ", inTree_name
        print "outFile_name = ", outFile_name
        
        ############
        
	gStyle.SetOptStat(0)

        # open the input toy MC
        data_sample = Fit.GetData([var[0] for var in variables], inFile_name, inTree_name)
        
        i_MLcD0 = -1
        i_MD0K = -1
        i_MLcK = 1
        
        # all the variables on which we are interested in
        variables_toadd = variables + variables_kin + other_variables
        
        # find the indeces of the invariant masses elements, in the 'variables_toadd' array
        # I know, it's clear I'm coming from the C++ school
        for i_var in range(0, len(variables_toadd)):
                if variables_toadd[i_var][0] == "MLcD0":
                        i_MLcD0 = i_var
                elif variables_toadd[i_var][0] == "MD0K":
                        i_MD0K = i_var
                elif variables_toadd[i_var][0] == "MLcK":
                        i_MLcK = i_var
        
        # check that everything went fine
        if (i_MLcD0 < 0) or (i_MD0K < 0) or (i_MLcK < 0) :
                print "Error: invariant masse indeces are not correct. i_MLcD0 = ", i_MLcD0, ", i_MD0K = ", i_MD0K, ", i_MLcK = ", i_MLcK
                print variables_toadd
                exit(1)
                
        phsp = PhaseSpace.PhaseSpace()
	data_ph = phsp.data_placeholder
        
        transform = Plot.AddVariables_ToSample(data_ph, phsp, variables_toadd)
        
        sess = tf.Session()
        
        data_sample = sess.run(transform, {data_ph: data_sample})
        
        ############
        ## LcD0 - D0K Dalitz plot
        ############
        
        # define the Dalitz plot
        plot = TH2F("dalitz_1", "",
                    70, variables_toadd[i_MD0K][1]**2 * 0.95, variables_toadd[i_MD0K][2]**2 * 1.05,
                    70, variables_toadd[i_MLcD0][1]**2 * 0.95, variables_toadd[i_MLcD0][2]**2 * 1.05)
        
        # set the axis
        plot.GetXaxis().SetTitle(variables_toadd[i_MD0K][3].replace("#it{m}", "#it{m}^{2}")
                                 + " [" + variables_toadd[i_MD0K][4].replace("eV", "eV^{2}").replace("#it{c}^{2}","#it{c}^{4}") + "]")
	plot.GetYaxis().SetTitle(variables_toadd[i_MLcD0][3].replace("#it{m}", "#it{m}^{2}")
                                 + " ["+variables_toadd[i_MLcD0][4].replace("eV", "eV^{2}").replace("#it{c}^{2}","#it{c}^{4}") + "]")

        # fill the Dalitz plot!
        for MLcD0, MD0K in zip(data_sample[:, i_MLcD0], data_sample[:, i_MD0K]):
		plot.Fill(MD0K**2, MLcD0**2)

        # draw and save the canvas
        can = TCanvas()

        plot.Draw("COLZ")
	can.SaveAs(outFile_name + "_1.pdf")

        ############
        ## LcD0 - LcK Dalitz plot
        ############

        # define the Dalitz plot
        plot2 = TH2F("dalitz_2", "",
                     70, variables_toadd[i_MLcK][1]**2 * 0.95, variables_toadd[i_MLcK][2]**2 * 1.05,
                     70, variables_toadd[i_MLcD0][1]**2 * 0.95, variables_toadd[i_MLcD0][2]**2 * 1.05)

        # set the axis titles
        plot2.GetXaxis().SetTitle(variables_toadd[i_MLcK][3].replace("#it{m}", "#it{m}^{2}")
                                  + " [" + variables_toadd[i_MLcK][4].replace("eV", "eV^{2}").replace("#it{c}^{2}","#it{c}^{4}") + "]")
        plot2.GetYaxis().SetTitle(variables_toadd[i_MLcD0][3].replace("#it{m}", "#it{m}^{2}")
                                  + " ["+variables_toadd[i_MLcD0][4].replace("eV", "eV^{2}").replace("#it{c}^{2}","#it{c}^{4}") + "]")

        # fill the Dalitz plot!
        for MLcD0, MLcK in zip(data_sample[:, i_MLcD0], data_sample[:, i_MLcK]):
                plot2.Fill(MLcK**2, MLcD0**2)

        # draw and save the canvas
        can2 = TCanvas()

        plot2.Draw("COLZ")
        can2.SaveAs(outFile_name + "_2.pdf")

        # save the output
	f = TFile.Open(outFile_name + "_ntuple.root", "RECREATE")
        Optimisation.FillNTuple("toy_MC", data_sample, [var[0] for var in variables_toadd])

        print "Toys saved to", f.GetName()

        f.Close()

