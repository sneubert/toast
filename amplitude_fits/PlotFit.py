#
# Copyright 2019 CERN for the benefit of the LHCb collaboration
#
# Licensed under the LCPGL License, Version 3 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License here:
#
#     https://opensource.org/licenses/LGPL-3.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IT IS" basis,
# without warranties or conditions of any kind, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Author: Alessio Piucci, alessio.piucci@cern.ch
#         (based on previous software by Adam Morris)
# ==============================================================================

import math
import MatrixElement
import PhaseSpace
import sys
import os
import argparse
import numpy as np
import importlib
import time

import tensorflow as tf
from tensorflow.python import debug as tf_debug

from ROOT import TFile, TH1F, TCanvas, TPad, TH1D, TLegend, TLine, gStyle

from variables_Lb2LcD0K import variables, other_variables

# import the Tensorflow stuff
from Interface import *
from Kinematics import *
import Optimisation
import ToyMC
import Utils

from Constants import *

# import the Python settings
from python_settings import *

def PullPlot(exphist, obshist):

        """""
          Plot the pulls
            exphist : histogram with expected data (i.e., the fit)
            obshist : histogram with observed data (i.e., the experimental data)
        """""

        # get a clone of the observed histo
        pullhist = obshist.Clone(obshist.GetName() + "pull")

        # subtract the expected data from the observed
        if(pullhist.Add(exphist, -1)):

                # loop over the bins
                for i in range(pullhist.GetNbinsX()):

                        # bin content = (observed - expected)/sqrt( d(observed)^2 + d(expected)^2 )
                        bincontent = pullhist.GetBinContent(i+1)

                        denominator = math.sqrt(obshist.GetBinError(i+1)**2 + exphist.GetBinError(i+1)**2)

                        if denominator == 0 :
                                logging.info("PullPlot:: Warning: (obs - exp) = {}, denominator = {}".format(bincontent, denominator))
                                denominator = 1.

                        bincontent /= denominator

                        pullhist.SetBinContent(i+1, bincontent)
                        pullhist.SetBinError(i+1, 0)

        # set some style
        pullhist.SetLineColor(kAzure + 3)
        pullhist.SetMarkerColor(kAzure + 3)
        pullhist.SetFillColor(kAzure + 3)
        pullhist.SetDrawOption("e1p")

        #newmax = max(pullhist.GetMaximum(), -pullhist.GetMinimum(), 4)
        #
        #pullhist.SetMaximum(newmax)
        #pullhist.SetMinimum(-newmax)
        
        pullhist.SetMaximum(6.)
        pullhist.SetMinimum(-6.)

        return pullhist
        
if __name__ == "__main__" :

        logging.info("")
        logging.info("-------------------------------")
        logging.info("----------- PlotFit -----------")
        logging.info("-------------------------------")
        
        ###########
        ## parser
        ###########

        parser = argparse.ArgumentParser(description="Parser of the PlotFit script")

        # get the input file and tree names of the data sample
        parser.add_argument('--inFile_data_name', dest = 'inFile_data_name',
                            default = 'toy.root', help = "name of the input data sample")
        parser.add_argument('--inTree_data_name', dest = 'inTree_data_name',
                            default = 'toy', help = "tree name of the input data sample")

        # get the input file and tree names of the normalisation sample
        parser.add_argument('--inFile_norm_name', dest = 'inFile_norm_name',
                            default = 'toy.root', help = "name of the input normalisation sample")
        parser.add_argument('--inTree_norm_name', dest = 'inTree_norm_name',
                            default = 'toy_normalisation', help = "tree name of the input normalisation sample")

        # get the input file with the fit fractions
        parser.add_argument('--inFile_fitfractions', dest = 'inFile_fitfractions',
                            default = 'FitToy_fractions.info', help = "name of the input file with the fit fractions")

        # get the input file with the values of the couplings
        parser.add_argument('--inFile_params', dest = 'inFile_params',
                            default = 'FitToy_couplings_cartesian.info', help = "name of the input file with values of the amplitude model parameters")
                
	# get the config file name where the resonances to be fitted are described
        parser.add_argument('--config_name', dest = 'config_name',
			    default = 'toy', help = "name of config file where the resonances are described")
		
        # set the output directory and file name
        parser.add_argument('--outFile_name', dest = 'outFile_name',
                            default = './plot_', help = "output file name")

        # get the names of the weights associated to the signal and normalisation events
        parser.add_argument('--sig_weight_name', dest='sig_weight_name',
                            default = "", help = "name of the weight assigned to the signal events. Keep it empty if they are not weighted")
        parser.add_argument('--norm_weight_name', dest='norm_weight_name',
                            default = "", help = "name of the weight assigned to the normalisation events. Keep it empty if they are not weighted")
        
        # in case you want to correct by an external efficiency factor,
        # additionally to what is described by the normalisation factor (like the BDT efficiency):
        # set the input file and histo names of the additional efficiency
        parser.add_argument('--inFileAdditionalEff_name', dest = 'inFileAdditionalEff_name',
                            default = "", help = "input file name of the additional efficiency factor")
        parser.add_argument('--inHistoAdditionalEff_suffix', dest = 'inHistoAdditionalEff_suffix',
                            default = "", help = "suffix of the histo name of the additional efficiency factor")

        parser.add_argument('--inFileAdditionalEff_2_name', dest = 'inFileAdditionalEff_2_name',
                            default = "", help = "input file name of the additional efficiency factor")
	parser.add_argument('--inHistoAdditionalEff_2_suffix', dest = 'inHistoAdditionalEff_2_suffix',
                            default = "", help = "suffix of the histo name of the additional efficiency factor")
        
        parser.add_argument('--inFilePlottingShape_name', dest = 'inFilePlottingShape_name',
                            default = "", help = "input file name containing the plotting shapes, used for the statistical uncertainties of the fit model")
                
        # number of phase-space events to simulate, to plot the fit model
        parser.add_argument('--nevents', dest = 'nevents',
                            default = 100000, help = "number of phase space events to simulate, to plot the fit model")
        
        # number of bins of the plots
        parser.add_argument('--nbins', dest = 'nbins',
                            default = 40, help = "number of bins")
        
        # now get the parsed options
        args = parser.parse_args()

        inFile_data_name = args.inFile_data_name
        inTree_data_name = args.inTree_data_name
        
        inFile_norm_name = args.inFile_norm_name
        inTree_norm_name = args.inTree_norm_name
        
        inFile_fitfractions = args.inFile_fitfractions
        inFile_params = args.inFile_params
        
	config_name = args.config_name
	
        outFile_name = args.outFile_name

        sig_weight_name = args.sig_weight_name
        norm_weight_name = args.norm_weight_name

        inFilePlottingShape_name = args.inFilePlottingShape_name
        
        inFileAdditionalEff_name = args.inFileAdditionalEff_name
        inHistoAdditionalEff_suffix = args.inHistoAdditionalEff_suffix
        
        inFileAdditionalEff_2_name = args.inFileAdditionalEff_2_name
	inHistoAdditionalEff_2_suffix = args.inHistoAdditionalEff_2_suffix
        
        nevents = int(args.nevents)
        nbins = int(args.nbins)
        
        # print the parsed options
        logging.info("")
        logging.info("inFile_data_name = {}".format(inFile_data_name))
        logging.info("inTree_data_name = {}".format(inTree_data_name))
        logging.info("inFile_norm_name = {}".format(inFile_norm_name))
        logging.info("inTree_norm_name = {}".format(inTree_norm_name))
        logging.info("inFile_fitfractions = {}".format(inFile_fitfractions))
        logging.info("inFile_params = {}".format(inFile_params))
        logging.info("config_name = {}".format(config_name))
        logging.info("outFile_name = {}".format(outFile_name))
        logging.info("nevents = {}".format(nevents))
        logging.info("nbins = {}".format(nbins))
        logging.info("sig_weight_name = {}".format(sig_weight_name))
        logging.info("norm_weight_name = {}".format(norm_weight_name))
        logging.info("inFileAdditionalEff_name = {}".format(inFileAdditionalEff_name))
        logging.info("inHistoAdditionalEff_suffix = {}".format(inHistoAdditionalEff_suffix))
        logging.info("inFileAdditionalEff_2_name = {}".format(inFileAdditionalEff_2_name))
	logging.info("inHistoAdditionalEff_2_suffix = {}".format(inHistoAdditionalEff_2_suffix))
        logging.info("inFilePlottingShape_name = {}".format(inFilePlottingShape_name))
        
        ############
	
        chunksize = 10000

        # let's kill the stats box
        gStyle.SetOptStat(0)
        
        # set and initialise the TensorFlow session
        config = tf.ConfigProto()
	config.gpu_options.allow_growth = True
        sess = tf.Session(config=config)

        start = time.time()
        
        # initialise the variables
        init = tf.global_variables_initializer()
	sess.run(init)

        # set a few sessions
        MatrixElement.sess = sess
        PhaseSpace.sess = sess
        Utils.sess = sess
        
        ########

        # import the resonances from the config file
        sys.path.append(os.path.dirname(config_name))
        resonance_module = importlib.import_module(os.path.basename(config_name)[:-3])  #-3 removes the '.py' at the end of the string

        non_res = resonance_module.non_res
        A = resonance_module.A
        B = resonance_module.B
        C = resonance_module.C
        M_d1_hel = resonance_module.M_d1_hel
        
	logging.info("")
        logging.info("Imported resonances:")
	logging.info("A = {}".format([A_res.name for A_res in A]))
	logging.info("B = {}".format([B_res.name for B_res in B]))
        logging.info("C = {}\n".format([C_res.name for C_res in C]))

        resonances = A + B + C

        # let's reinitialise the variables
        init = tf.global_variables_initializer()
        sess.run(init)
        
        data_legend = resonance_module.data_legend

        # open the file with the additional efficiency factor... if you have to do so
        inFileAdditionalEff = None
        inFileAdditionalEff_2 = None
        
        if inFileAdditionalEff_name != "" :
                inFileAdditionalEff = TFile.Open(inFileAdditionalEff_name)

                # check that the file exists
	        if not inFileAdditionalEff :
                        logging.error("Error: the input file of the additional efficiency does not exist.\n")
			sys.exit(1)

        if inFileAdditionalEff_2_name != "" :
                inFileAdditionalEff_2 = TFile.Open(inFileAdditionalEff_2_name)
                
		# check that the file exists
                if not inFileAdditionalEff_2 :
			logging.error("Error: the input file of the (second) additional efficiency does not exist.\n")
                        sys.exit(1)
                        
        # open the file with the plotting shapes... if you have to do so
        inFilePlottingShape = None
        
        if inFilePlottingShape_name != "" :
                
                # open the file with the plotting shapes                                                                                                                                      
                inFilePlottingShape = TFile.Open(inFilePlottingShape_name)
        
                # check that the file exists                                                                                                                                                  
                if not inFilePlottingShape :
                        logging.error("Error: the input file of the plotting shapes.\n")
                        sys.exit(1)

        #############

        # update the amplitude model with the parameters taken from an external file:
        # couplings, non-resonant size, masses and widths of the resonances
        #resonances, non_res = Utils.UpdateModel(resonances = resonances, non_res = non_res,
        #                                        inFile_params = inFile_params)
        
        #############
        
        # create the phase space
        phsp = PhaseSpace.ThreeBodyPhaseSpace()
        
        # list of switches (flags that control the components of the PDF)
        # +1 stands for the non-resonant contribution
        switches = Optimisation.Switches(len(resonances) + 1)
        
        def event_weight(datapoint, norm = 1.):
                return tf.transpose(datapoint)[-1] * norm # dangerous: assume that the weight is the last element of the list

        def model(datapoint, component_index = -1):
                return MatrixElement.MatrixElement(phsp, datapoint, switches,
                                                   component_index, resonances = resonances,
                                                   non_res = non_res, M_d1_hel_ = M_d1_hel)
        
        # build the PDFs
        data_ph = phsp.data_placeholder
        norm_ph = phsp.norm_placeholder

        data_pdf = model(data_ph)
        norm_pdf = model(norm_ph)

        #############

        # That's how the plotting will be performed:
        # 1) use the fitted fit fractions to generate with toy simulations
        #    the contributions of the single fit components
        # 2) use the input data to show the data points in the final plots
        # 3) define the efficiency has ratio between the normalisation sample
        #    (with all the data selection applied to it) and a uniform sample generated with toy simulations
        # 3b) optional: introduce an additional efficiency factor from an external histo,
        #     for efficiency effects not described by the normalisation sample
        #     (like the BDT efficiency)
        # 4) scale the fit curves by the efficiency profiles
        
        #############

        input_weight_name = []

        if sig_weight_name != "":
                input_weight_name = [sig_weight_name]
        
        # get the data, and the sweights
        unfiltered_data_sample = Utils.GetData([var[1] for var in variables] + input_weight_name,
                                               inFile_data_name, inTree_data_name)
        
        # filter the data, checking for the allowed phase space
        data_sample = sess.run(phsp.Filter(unfiltered_data_sample))

        input_weight_name = []

        if norm_weight_name != "":
                input_weight_name = [norm_weight_name]
                
        # get the normalisation sample, and the weights
        unfiltered_norm_sample = Utils.GetData([var[1] for var in variables] + input_weight_name,
                                               inFile_norm_name, inTree_norm_name)
        
        # filter the normalisation sample, checking for the allowed phase space
        norm_sample = sess.run(phsp.Filter(unfiltered_norm_sample))

        # get the fit fractions
        fit_fractions = Optimisation.ReadFitFractions(inFile_fitfractions,
                                                      [res.name for res in resonances] +  ["non-resonant"])
        
        # print the B_LS couplings of the resonances, for debug
        for res in resonances:
                Utils.print_BLS(res)

        ########
        # start with the fun: plot the fit projections
        ########

        majorant = ToyMC.EstimateMaximum(sess, data_pdf, data_ph, norm_sample) * 1.1

        # generate a fit sample, i.e. a toy MC with the input fit fractions
        fit_sample = ToyMC.RunToyMC(sess, data_pdf, data_ph, phsp,
                                    nevents, majorant, chunk = chunksize,
                                    seed = 1000)
        
        # components of the fit model
        component_samples = []

        # loop over the fit components to generate
        for i in range(len(fit_fractions)):
                
                name = Utils.getname(i, resonances)
                
                # only generate the components with non null fit fractions
                if int(len(fit_sample)*fit_fractions[i]) > 0:
                        
                        logging.info("Generating the {} component".format(name))

                        # build the PDF
                        component_pdf = model(data_ph, i)

                        majorant = ToyMC.EstimateMaximum(sess, component_pdf, data_ph, norm_sample) * 1.1

                        # generate the component
                        component_samples += [ToyMC.RunToyMC(sess, component_pdf, data_ph, phsp,
                                                             int(len(fit_sample) * fit_fractions[i]),
                                                             majorant, chunk = chunksize,
                                                             seed = 1000)]
                else:
                        logging.info("Won't generate toys for {} since the fit fraction is too small = {}".format(name, str(fit_fractions[i])))
                        
                        component_samples += [[[]]]
        
        # let's add the other variables that you want to plot
        variables += other_variables
        
        # components to be plotted (only the ones with non null fit fraction)
        components_tobe_plotted = []

        ###########
        # build the data sample (data points) and the fit sample (fit curves)
        ###########

        # fill the data sample showing the input data
        if sig_weight_name != "" :
                # define a weighted sample that will be filled with the input data
                # the weight is the one in the input dataset (aka the sweight, for the fitted data)
                sample_with_vars = Utils.AddVariables_ToSample(data_ph, phsp, variables, weighted = True)
        else :
                # un-weighted sample
                sample_with_vars = Utils.AddVariables_ToSample(data_ph, phsp, variables, weighted = False)
                
        # fill the data sample: it will represent the data points in the final plots
        data_sample = sess.run(sample_with_vars, {data_ph: data_sample})
        
        # define a not-weighted sample (i.e., with a weight == 1.)
        sample_forfit = Utils.AddVariables_ToSample(data_ph, phsp, variables, weighted = False)

        # fill the fit sample: it will represent the fit curve in the final plots 
        fit_sample = sess.run(sample_forfit, {data_ph: fit_sample})

        ###########

        # fill the single components
        for i in range(len(component_samples)):
                name = Utils.getname(i, resonances)

                if len(component_samples[i]) > 1:

                        # add to the list of components to be plotted
                        if i < len(resonances) :
                                components_tobe_plotted += [resonances[i].label]
                        else :
                                components_tobe_plotted += ["non-resonant"]

                        component_samples[i] = sess.run(sample_forfit, {data_ph: component_samples[i]})
                else:
                        logging.info("Skipping {}".format(name))

        # fill the normalisation sample
        if norm_weight_name != "" :
                # define a weighted sample that will be filled with the input data
                # the weight is the one in the input dataset (aka the sweight, for the fitted data)
                sample_with_vars = Utils.AddVariables_ToSample(norm_ph, phsp, variables, weighted = True)
        else :
                # un-weighted sample
                sample_with_vars = Utils.AddVariables_ToSample(norm_ph, phsp, variables, weighted = False)

        norm_sample = sess.run(sample_with_vars, {norm_ph: norm_sample})
        
        # generate a uniform sample
        unif_sample = sess.run(phsp.UniformSample(nevents))
        
        unif_sample = sess.run(sample_forfit, {data_ph: unif_sample})
        
        # loop over the variables to plot
        for i in range(len(variables)):

                var = variables[i]

                # define the canvas
                can = TCanvas(var[1] + "can", "", 800, 600)
                can.Draw()
                can.cd()

                ylow_pad = 0.

                # set the lower y of the pad accordingly to the pulls pad
                if inFilePlottingShape == None :
                        # no uncertainties --> no pulls
                        ylow_pad = 0.075
                else :
                        # uncertainties --> get space for the pulls
                        ylow_pad = 0.2
                                
                # define the main pad
                mainpad = TPad(can.GetName() + "main", "", 0.0, ylow_pad, 1.0, 1.)

                # with pulls
                mainpad.SetMargin(0.11, 0.05, 0.03, 0.05)
                        
                mainpad.Draw()
                mainpad.cd()
                Utils.StyleCanvas(can)
                
                # histo with the data points
                data_hist = TH1D(var[1] + "data", "", nbins, var[2], var[3])

                # histo with the fit curves
                fit_hist = TH1D(var[1] + "model", "", nbins, var[2], var[3])

                # efficiency histo
                eff_hist = TH1D(var[1] + "norm", "", nbins, var[2], var[3])

                # whatever histo
                unif_hist = TH1D(var[1] + "unif", "", nbins, var[2], var[3])

                # array of histos for the single fit components
                # add a separate histo for each of the component
                comp_hists = []
                
                for j in range(len(fit_fractions)):
                        name = Utils.getname(j, resonances)

                        # define a new histo
                        comp_hists += [TH1D(var[1] + name, "", nbins, var[2], var[3])]

                # fill all the histos in two shots
                for hist, sample in [[data_hist, data_sample],
                                     [fit_hist, fit_sample],
                                     [eff_hist, norm_sample],
                                     [unif_hist, unif_sample]]:
                        for dp in sample:
                                hist.Fill(dp[i], dp[-1])

                for j in range(len(component_samples)):
                        if len(component_samples[j]) > 1:
                                for dp in component_samples[j][:,i]:
                                        comp_hists[j].Fill(dp)

                                Utils.StyleComponentHist(comp_hists[j], j, resonances)

                # get the statistical uncertainties of the fit model from the external plotting shapes
                # otherwise, don't set any uncertainty
                if inFilePlottingShape != None :
                        
                        # get the shape of the current variable
                        shape_histo = inFilePlottingShape.Get(var[1])
                        
                        # check that the histos with the fit model and the plotting shape
                        # have same range and binning
                        if ((shape_histo.GetXaxis().GetXmin() != fit_hist.GetXaxis().GetXmin())
                            or (shape_histo.GetXaxis().GetXmax() != fit_hist.GetXaxis().GetXmax())
                            or (shape_histo.GetNbinsX() != fit_hist.GetNbinsX())) :
                                logging.error(("Error: the fit histo and plotting shape of {} "
                                               + "are not compatible".format(var[1])))
                                logging.error("shape_histo.GetNbinsX() = {}, fit_hist.GetNbinsX() = {}".format(shape_histo.GetNbinsX(), fit_hist.GetNbinsX()))
                                exit(1)

                        # loop over the bins of the histos
                        for i_bin in range(1, shape_histo.GetNbinsX() + 1) :

                                # I know that...
                                if shape_histo.GetBinContent(i_bin) != 0: 
                                        # get the relative statistical uncertainty from the plotting shape
                                        rel_uncertainty = shape_histo.GetBinError(i_bin) / shape_histo.GetBinContent(i_bin)
                                        
                                        #logging.info("old {}, new {}".format(fit_hist.GetBinError(i_bin), rel_uncertainty * fit_hist.GetBinContent(i_bin)))
                                
                                        # set the statistical uncartinaty to the fit histo
                                        fit_hist.SetBinError(i_bin, rel_uncertainty * fit_hist.GetBinContent(i_bin))
                                else :
                                        fit_hist.SetBinError(i_bin, 0.)
                        
                # get the external efficiency factors not considered in the normalisation sample
                AdditionalEffHisto = None
                AdditionalEffHisto_2 = None
                
                if inFileAdditionalEff != None :
                        inHistoEff_name = var[1] + inHistoAdditionalEff_suffix
                        
                        # open the efficiency histo
                        AdditionalEffHisto = inFileAdditionalEff.Get(inHistoEff_name)
                        
                        # check that the histo exists
                        if not AdditionalEffHisto :
                                logging.error("Error: the efficiency histo {} does not exist.\n".format(inHistoEff_name))
                                sys.exit(1)
                                
                if inFileAdditionalEff_2 != None :
                        inHistoEff_2_name = var[1] + inHistoAdditionalEff_2_suffix
                        
                        # open the efficiency histo
                        AdditionalEffHisto_2 = inFileAdditionalEff_2.Get(inHistoEff_2_name)

                        # check that the histo exists
			if not AdditionalEffHisto_2 :
                                logging.error("Error: the efficiency histo {} does not exist.\n".format(inHistoEff_2_name))
				sys.exit(1)
                                
                # calculate the efficiency described by the normalisation factor
                # eff = normalisation sample / uniform sample
                # the normalisation and uniform histos must be normalised before
                eff_hist.Scale(1./eff_hist.Integral())
                unif_hist.Scale(1./unif_hist.Integral())
                eff_hist.Divide(unif_hist)
                
                # now multiply the efficiency of the normalisation factor by the external efficiency factors
                if AdditionalEffHisto != None :
                        # rebin the efficiency histo and multiply it to the other efficiencies
                        #AdditionalEffHisto.Rebin(AdditionalEffHisto.GetNbinsX() / nbins)
                        eff_hist.Multiply(AdditionalEffHisto)

                if AdditionalEffHisto_2 != None :
                        # rebin the efficiency histo and multiply it to the other efficiencies
                        #AdditionalEffHisto_2.Rebin(AdditionalEffHisto_2.GetNbinsX() / nbins)
		        eff_hist.Multiply(AdditionalEffHisto_2)
                
                # rescale the efficiency histo: its integral should be the number of bins
                if (AdditionalEffHisto != None) or (AdditionalEffHisto_2 != None) :
                        eff_hist.Scale(eff_hist.GetNbinsX() / eff_hist.Integral())

                # normalisation factor to overimpose the data point and the fit curves
                scale = data_hist.Integral()/fit_hist.Integral()
                
                # some style
                fit_hist.SetLineColor(2)
                fit_hist.SetLineWidth(2)
                
                Utils.StyleHist(data_hist, var[4], var[5])
                data_hist.SetLineColor(1)
                data_hist.SetMarkerStyle(20)

                data_hist.GetXaxis().SetLabelSize(0)

                data_hist.GetYaxis().SetTitleSize(0.055)
                data_hist.GetYaxis().SetTitleOffset(0.9)
                data_hist.GetYaxis().SetLabelSize(0.05)

                data_hist.Draw("E1")
                data_hist.SetMinimum(0)

                # where to put the legend: right or left side?
                # right is the default
                x_min = 0.72
                x_max = 0.9
                
                if var[6] == "left" :
                        x_min = 0.15
                        x_max = 0.33
                
                #define the legend
                legend = TLegend(x_min, 0.2, x_max, 0.9, "LHCb internal", "brNDC")

                legend.AddEntry(data_hist, data_legend, "elp")

                # loop over the histos of the single fit contributions + histo with the total fit
                for hist in [fit_hist]+comp_hists:
                        
                        # to select the components which should be plotted
			to_be_plotted = False

			if "model" in hist.GetName() :
                                legend.AddEntry(hist, "model", "l")
                                to_be_plotted = True
			else :
                                for comp in components_tobe_plotted :
                                        if comp in hist.GetName() :
                                                legend.AddEntry(hist, hist.GetName(), "f")
		                                to_be_plotted = True

			if to_be_plotted :
                                #print "Plotting hist =", hist.GetName()
                                
                                # multiply the fit model and contributions by the efficiency
                                hist.Multiply(eff_hist)
		                hist.Scale(scale)

                                # if no shapes from toys have been used, put to 0. all the uncertainties
                                if ("model" in hist.GetName()) and (inFilePlottingShape == None) :
                                        for i_bin in range(1, hist.GetNbinsX() + 1) :
                                                hist.SetBinError(i_bin, 0.)
                                
				hist.Draw("h same")

                legend.SetBorderSize(0);
                legend.SetFillColor(kWhite);
                legend.SetTextAlign(12);
                legend.SetFillStyle(0);

                legend.Draw()

                can.cd()

                # plot the pull distribution
                pullpad = TPad(can.GetName() + "pull", "", 0.0, 0., 1.0, 0.21)
                pullpad.SetMargin(0.11, 0.05, 0.4, 0.05)
                pullpad.Draw()
                pullpad.cd()
                
                # compute the pulls
                pullplot = PullPlot(fit_hist, data_hist)
                
                Utils.StyleHist(pullplot, var[4], var[5])
                
                pullplot.GetYaxis().SetTitle("Pull")
                
                pullplot.GetXaxis().SetTitleSize(0.2)
                pullplot.GetXaxis().SetTitleOffset(0.8)
                
                pullplot.GetXaxis().SetLabelSize(0.18)
                
                pullplot.GetYaxis().SetTitleSize(0.2)
                pullplot.GetYaxis().SetTitleOffset(0.25)
                pullplot.GetYaxis().SetLabelSize(0.18)
                
                pullplot.Draw("BX")
                
                #draw a horizontal line, corresponding to +3 sigma
                plus_three_sigma = TLine(pullplot.GetXaxis().GetXmin(), 3.,   #xmin, ymin
                                         pullplot.GetXaxis().GetXmax(), 3.)   #xmax, ymax
                
                #red, dashed line
                plus_three_sigma.SetLineStyle(7)
                plus_three_sigma.SetLineColor(kBlack)
                plus_three_sigma.Draw("same")
                
                #draw a horizontal line, corresponding to -3 sigma
                minus_three_sigma = TLine(pullplot.GetXaxis().GetXmin(), -3.,   #xmin, ymin
                                          pullplot.GetXaxis().GetXmax(), -3.)   #xmax, ymax 
                
                #red, dashed line
                minus_three_sigma.SetLineStyle(7)
                minus_three_sigma.SetLineColor(kBlack)
                minus_three_sigma.Draw("same")

                # if there are not uncertainties on the fit model,
                # overimpose the fit pad to the pulls
                if inFilePlottingShape == None :
                        fit_hist.Draw("same")
                        
                #########
                
                #data_hist.SetMaximum(max(data_hist.GetMaximum(), fit_hist.GetMaximum())*1.1)
                data_hist.GetYaxis().SetRangeUser(0., max(data_hist.GetMaximum(), fit_hist.GetMaximum())*1.1)
                
                # replot the main pad, to draw correctly the axis
                can.cd()
                mainpad.Draw()

                # save the plot to an external file
                out_path = outFile_name + "_" + var[1] + ".pdf"

                logging.info("Plotting {}".format(out_path))

                can.SaveAs(out_path)

                ##########

                # draw and save the efficiency plot as well
                can.Clear()

                Utils.StyleHist(eff_hist, var[4], var[5])
                eff_hist.SetLineColor(1)
                eff_hist.SetMarkerStyle(20)

                eff_hist.GetXaxis().SetLabelSize(0)

                eff_hist.GetYaxis().SetTitle("efficiency")
                eff_hist.GetYaxis().SetTitleSize(0.055)
                eff_hist.GetYaxis().SetTitleOffset(0.9)
                eff_hist.GetYaxis().SetLabelSize(0.05)

                eff_hist.SetMinimum(0.5)
                eff_hist.SetMaximum(1.5)
                
                can.cd()
                eff_hist.Draw()
                
                out_path = outFile_name + "_" + var[1] + "_efficiency.pdf"
                
                can.SaveAs(out_path)

        # print out the total execution time
        end = time.time()
        logging.info("PlotFit:: Execution time = {} min".format(str((end - start)/60.)))

        # finally close the session
        sess.close()
