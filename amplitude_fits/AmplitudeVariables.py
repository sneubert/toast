#
# Copyright 2019 CERN for the benefit of the LHCb collaboration
#
# Licensed under the LCPGL License, Version 3 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License here:
#
#     https://opensource.org/licenses/LGPL-3.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IT IS" basis,
# without warranties or conditions of any kind, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Author: Alessio Piucci, alessio.piucci@cern.ch
# ==============================================================================

import math
import sys
import argparse
import importlib
import itertools

import tensorflow as tf
from tensorflow.python import debug as tf_debug

import numpy as np

from ROOT import TFile, TTree, TBranch, TVector3, TLorentzVector, TMath
import array

# import the TensorflowAnalysis libraries
import Optimisation
import Kinematics
from Interface import Cos, Const, Sqrt, Ones

# import the local libraries
import PhaseSpace

# import the Python settings
from python_settings import *

if __name__ == "__main__" :

        logging.info("")
        logging.info("--------------------------------")
        logging.info("------ AmplitudeVariables ------")
        logging.info("--------------------------------")
        
        ###########
        ## parser
        ###########

        parser = argparse.ArgumentParser(description="Parser of the AmplitudeVariables script")

        # get the input file and tree names of the sample
        parser.add_argument('--inFile_name', dest = 'inFile_name',
                            default = 'toy.root', help = "name of the input sample")
        parser.add_argument('--inTree_name', dest = 'inTree_name',
                            default = 'toy', help = "tree name of the input sample")
        
        # set the output file name
        parser.add_argument('--outFile_name', dest = 'outFile_name',
                            default='./', help = "output file name")

        # input data format
        parser.add_argument('--input_format', dest = 'input_format',
                            default='LHCbMC', help = "input data format")

        # specify if you want to save a weight from the input file
        # to the output sample. In case you don't, a weight == 1. for all events will be saved
        parser.add_argument('--weight_name', dest = 'weight_name',
                            default='', help = "name of the event weights")

        parser.add_argument('--additional_variables', dest = 'additional_variables',
                            default='', help = "additional variables to be saved in the output tree")
        
        # now get the parsed options
        args = parser.parse_args()

        inFile_name = args.inFile_name
        inTree_name = args.inTree_name
        outFile_name = args.outFile_name
        input_format = args.input_format
        weight_name = args.weight_name
        additional_variables = args.additional_variables
        
        # print the parsed options
        logging.info("\n")
        logging.info("inFile_name = {}".format(inFile_name))
        logging.info("inTree_name = {}".format(inTree_name))
        logging.info("outFile_name = {}".format(outFile_name))
        logging.info("input_format = {}".format(input_format))
        logging.info("weight_name = {}".format(weight_name))
        logging.info("additional_variables = {}".format(additional_variables))
        
        # initialise the TensorfFlow session
	config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        
        sess = tf.Session(config=config)

        # initialise the variables
	init = tf.global_variables_initializer()
        sess.run(init)

        # phase space object, where the coordinates transformations are defined
        phase_space = PhaseSpace.ThreeBodyPhaseSpace()
        PhaseSpace.sess = sess

        # branches to read from the input file, depending on the input data format
        branches = []

        # the labels of the branches are LHCb-(and channel-)specific:
        # simply modify them if you need
        if input_format == "LHCbMC" :
                branches = (["Lc_TRUEP_X","Lc_TRUEP_Y", "Lc_TRUEP_Z", "Lc_TRUEP_E"]
                            + ["D0_TRUEP_X", "D0_TRUEP_Y", "D0_TRUEP_Z", "D0_TRUEP_E"]
                            + ["lab8_TRUEP_X", "lab8_TRUEP_Y", "lab8_TRUEP_Z", "lab8_TRUEP_E"])
        elif input_format == "LHCbData" :
                branches = (["Lb_ConsLb_Lc_PX","Lb_ConsLb_Lc_PY", "Lb_ConsLb_Lc_PZ", "Lb_ConsLb_Lc_PE"]
                            + ["Lb_ConsLb_D0_PX", "Lb_ConsLb_D0_PY", "Lb_ConsLb_D0_PZ", "Lb_ConsLb_D0_PE"]
                            + ["Lb_ConsLb_K_PX","Lb_ConsLb_K_PY", "Lb_ConsLb_K_PZ", "Lb_ConsLb_K_PE"])

        # get the amplitude variables of the A decay chain + inv. masses
        A_coordinates_branches = ["MLcD0", "cosThetaLb", "cosThetaA", "phiLc", "MD0K", "MLcK"]
        branches += A_coordinates_branches
        
        additional_branches = []
        num_additional_branches = 0
        
        # add the additional variables, if needed
        if additional_variables != "":
                additional_branches = additional_variables.split(':') #np.core.defchararray.split(additional_variables, sep = ":")
                num_additional_branches = len(additional_branches)

                logging.debug("additional_branches = {}".format(additional_branches))
                
                branches += additional_branches
        
        # add the external weight, if needed
        if weight_name != "" :
                branches += [weight_name]
                
        # open the input file
        inFile = TFile.Open(inFile_name, "read")

        # check that the file exists
        if not inFile :
                logging.error("Error: the input file does not exist.")
                exit(1)

        # open the input tree
        inTree = inFile.Get(inTree_name)

        if not inTree :
                logging.error("Error: the input tree does not exist.")
                exit(1)
        
        # clone the input tree
        outTree = inTree.CloneTree(0)
        
        # get the data
        sample = Optimisation.ReadNTuple(inTree, branches)

        # get the values of the A coordinates
        A_coordinates = np.array(sample[:,12:18])
        
        # take the momentum components from the sample, and convert them from MeV to GeV
        sample_momentum = 0.001 * np.array(sample[:,:12])
        
        logging.debug("sample[0] = {}".format(sample[0]))
        logging.debug("sample_momentum[0] = {}".format(sample_momentum[0]))
        
        # fill the Lc, D0, K qvects
        Lc_px = tf.transpose(sample_momentum)[0]
        Lc_py = tf.transpose(sample_momentum)[1]
        Lc_pz = tf.transpose(sample_momentum)[2]
        Lc_E  = tf.transpose(sample_momentum)[3]
        
        D0_px = tf.transpose(sample_momentum)[4]
        D0_py = tf.transpose(sample_momentum)[5]
        D0_pz = tf.transpose(sample_momentum)[6]
        D0_E  = tf.transpose(sample_momentum)[7]
        
        K_px = tf.transpose(sample_momentum)[8]
        K_py = tf.transpose(sample_momentum)[9]
        K_pz = tf.transpose(sample_momentum)[10]
        K_E  = tf.transpose(sample_momentum)[11]
        
        # build the 4-momenta
        Lc_qvect = Kinematics.LorentzVector(Kinematics.Vector(Lc_px, Lc_py, Lc_pz), Lc_E)
        D0_qvect = Kinematics.LorentzVector(Kinematics.Vector(D0_px, D0_py, D0_pz), D0_E)
        K_qvect = Kinematics.LorentzVector(Kinematics.Vector(K_px, K_py, K_pz), K_E)
                
        logging.debug("A_coordinates = {}".format(A_coordinates))
        
        ###########
        
        # compute the amplitude variables for the B and C chains,
        # using the 4-momenta components
        B_coordinates = phase_space.BCoordinates(A_coordinates, d1_qvect = Lc_qvect, d2_qvect = D0_qvect, d3_qvect = K_qvect)
        C_coordinates = phase_space.CCoordinates(A_coordinates, d1_qvect = Lc_qvect, d2_qvect = D0_qvect, d3_qvect = K_qvect)
        
        logging.debug("B_coordinates = {}".format(sess.run(B_coordinates)))
        logging.debug("C_coordinates = {}".format(sess.run(C_coordinates)))

        # compute the amplitude variables for the B and C chains,
        # NOT using the 4-momenta components but MLcD0, cosThetaA, cosThetaLb, phiLc
        # this is just for debug
        B_coordinates_debug = phase_space.BCoordinates(A_coordinates)
        C_coordinates_debug = phase_space.CCoordinates(A_coordinates)
        
        # define the final sample of A, B, C coordinates to write out
        final_sample = np.array((sess.run(Sqrt(B_coordinates["m2_d2d3"])),
                                 sess.run(Cos(B_coordinates["theta_M_B"])),
                                 sess.run(Cos(B_coordinates["theta_B"])),
                                 sess.run(B_coordinates["phi_d1_B"]),
                                 sess.run(B_coordinates["phi_d2_B"]),
                                 sess.run(Sqrt(C_coordinates["m2_d1d3"])),
                                 sess.run(Cos(C_coordinates["theta_M_C"])),
                                 sess.run(Cos(C_coordinates["theta_C"])),
                                 sess.run(C_coordinates["phi_d2_C"]),
                                 sess.run(C_coordinates["phi_d1_C"]),
                                 sess.run(Sqrt(B_coordinates_debug["m2_d2d3"])),
                                 sess.run(Sqrt(C_coordinates_debug["m2_d1d3"])),
                                 (sample[:,-1] if weight_name != "" else sess.run(Ones(B_coordinates["m2_d2d3"])))))  # add the weights, or just a lot of 1.
        
        final_sample = np.transpose(final_sample)
        
        # add the amplitude variables of the A chain + the additional variables to the sample
        additional_values = np.array(sample[:, 12:18+num_additional_branches])    # 18 = 12 momentum components + 6 amplitude variables from the input
        final_sample = np.concatenate((final_sample, additional_values), axis=1)
                
        logging.debug("final_sample = {}".format(final_sample))
                                              
        # assign a default name to the weight,
        # if it's not taken from the input file
        if weight_name == "":
                weight_name = "weight"
                
        # create the output file
        outFile = TFile.Open(outFile_name, "recreate")
        
        # list of the output branches
        out_branches = (["MD0K_2", "cosThetaLb_B", "cosThetaB", "phiLc_B", "phiD0_B",
                         "MLcK_2", "cosThetaLb_C", "cosThetaC", "phiD0_C", "phiLc_C",
                         "MD0K_debug", "MLcK_debug", weight_name]
                        + A_coordinates_branches + additional_branches)
        
        # write out the new variables to the output tree
	# the label of the branches are LHCb-(and channel-) specific:
        # simply modify them if you need
        Optimisation.FillTree_double(outFile, inTree_name, final_sample, out_branches)

        # write the output file
        #outFile.Write()
        outFile.Close()

        # run away!
        sess.close()
        
        # write out the new variables to the output tree
        # the label of the branches are LHCb-(and channel-) specific:
        # simply modify them if you need
        #Optimisation.FillNTuple(inTree_name, final_sample, out_branches)
        
