
# Configuration file for fitting a Lb2LcD0K amplitude model with a bunch of Ds* and Xic states
#
# Alessio Piucci

import sys

#import the local libraries
from Constants import *
import MatrixElement
import Optimisation
from Interface import Const
from Optimisation import FitParameter

# fit the non-resonant component
non_res = MatrixElement.NonResonant(nonres_size = FitParameter("nonres_size", 0.05, 0., 0.2, 0.001),
                                    colour = kPink, linestyle = 7, fillstyle = 3954)

# M and d1 polarisation factors
M_d1_hel = None

#M_d1_hel = {}
#M_d1_hel[1] = {}
#M_d1_hel[1][1] = Const(0.5)
#M_d1_hel[1][-1] = Const(0.5)
#
#M_d1_hel[-1] = {}
#M_d1_hel[-1][1] = Const(0.5)
#M_d1_hel[-1][-1] = Const(0.5)

# list of Lc D0 resonances
A = [
]

# list of D0 K resonances
B = [
    
    # Lb --> Lc Ds2*(2573)
    MatrixElement.BResonanceBreitWigner(name = Ds2_2573.name, label = Ds2_2573.label,
                                        mass = Ds2_2573.mass, width = Ds2_2573.width,
                                        spin = Ds2_2573.spin, parity = Ds2_2573.parity, aboveLmin_M = 0,
                                        BLS_couplings_dict_M = {
                                            "Ds2_2573__M_Bs3l2_Real" : FitParameter("Ds2_2573__M_Bs3l2_Real", 1., -15., 15., 0.01),
                                            "Ds2_2573__M_Bs3l2_Imag" : FitParameter("Ds2_2573__M_Bs3l2_Imag", 1., -15., 15., 0.01),
                                        },
                                        BLS_couplings_dict_res = {
                                            "Ds2_2573__res_Bs0l4_Real" : FitParameter("Ds2_2573__res_Bs0l4_Real", 1., -15., 15., 0.01),
                                            "Ds2_2573__res_Bs0l4_Imag" : FitParameter("Ds2_2573__res_Bs0l4_Imag", 1., -15., 15., 0.01),
                                        },
                                        resdecay_parity_conservation = True,
                                        colour = kAzure, linestyle = 7, fillstyle = 3954),
    
    # Lb --> Lc Ds*1(2700)
    MatrixElement.BResonanceBreitWigner(name = Ds1_2700.name, label = Ds1_2700.label,

                                        # fit the mass and widths of this contribution
                                        mass = FitParameter("Ds1_2700__mass", 2.708, 2.6, 2.72, 0.001),
                                        width = FitParameter("Ds1_2700__width", 0.1, 0.05, 0.25, 0.001),
                                        spin = Ds1_2700.spin, parity = Ds1_2700.parity, aboveLmin_M = 0,
                                        BLS_couplings_dict_M = {
                                            "Ds1_2700__M_Bs1l0_Real" : FitParameter("Ds1_2700__M_Bs1l0_Real", 1., -5., 15., 0.01),
                                            "Ds1_2700__M_Bs1l0_Imag" : FitParameter("Ds1_2700__M_Bs1l0_Imag", 1., -5., 15., 0.01),
                                        },
                                        BLS_couplings_dict_res = {
                                            "Ds1_2700__res_Bs0l2_Real" : FitParameter("Ds1_2700__res_Bs0l2_Real", 1., -5., 5., 0.01),
                                            "Ds1_2700__res_Bs0l2_Imag" : FitParameter("Ds1_2700__res_Bs0l2_Imag", 1., -5., 5., 0.01),
                                        },
                                        resdecay_parity_conservation = True,
                                        colour = kBrown, linestyle = 7, fillstyle = 3954),
    
]

# list of Lc K resonances
C = [
    # Lb --> Xic(2815) D0
    MatrixElement.CResonanceBreitWigner(name = Xic_2815.name, label = Xic_2815.label,
                                        mass = Xic_2815.mass, width = Const(0.030),
                                        spin = Xic_2815.spin, parity = Xic_2815.parity, aboveLmin_M = 0,
                                        BLS_couplings_dict_M = {
                                            "Xic_2815__M_Bs3l2_Real" : FitParameter("Xic_2815__M_Bs3l2_Real", 1., -5., 5., 0.01),
                                            "Xic_2815__M_Bs3l2_Imag" : FitParameter("Xic_2815__M_Bs3l2_Imag", 1., -5., 5., 0.01),
                                        },
                                        BLS_couplings_dict_res = {
                                            "Xic_2815__res_Bs1l4_Real" : FitParameter("Xic_2815__res_Bs1l4_Real", 1., -5., 5., 0.01),
                                            "Xic_2815__res_Bs1l4_Imag" : FitParameter("Xic_2815__res_Bs1l4_Imag", 1., -5., 5., 0.01),
                                        },
                                        resdecay_parity_conservation = True,
                                        colour = kMagenta, linestyle = 7, fillstyle = 3954),
]
