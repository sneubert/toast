
# default configuration for CENTOS 7

# to point to the miniconda2 Python installations (setting ROOT later, seems to superseed them)
export PYTHON="$(which python)"
export PYTHONTWO="$(which python2)"

# set GCC 6.2 from cvmfs
export PATH="/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_91/gcc/6.2.0/x86_64-centos7/bin/:$PATH"
export LD_LIBRARY_PATH="/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_91/gcc/6.2.0/x86_64-centos7/lib64/:$LD_LIBRARY_PATH"

# set a proper ROOT version to use
export ROOT_INSTALLATION=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_91/ROOT/6.10.06/x86_64-centos7-gcc62-opt/bin/thisroot.sh

# for ROOT: set a valid GSL installation
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_91/GSL/2.1/x86_64-centos7-gcc7-opt/lib/

# for GSL: this might be not needed
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_91/GSL/2.1/x86_64-centos7-gcc7-opt/include/gsl

# source ROOT
source $ROOT_INSTALLATION

# source the miniconda2 Python installation
alias python=$PYTHON
alias python2=$PYTHONTWO
alias python2.7=$PYTHONTWO

##################

# add some folders where the required Python modules are placed
export PYTHONPATH=$PYTHONPATH:`pwd`/externals/TensorFlowAnalysis/TensorFlowAnalysis/:`pwd`/config/:`pwd`/config/amplitude_fits/:`pwd`/include/:`pwd`/config/amplitude_fits/states

# activate snake
source activate snake

# test the TensorFlow installation
python2.7 test_TensorFlow.py
