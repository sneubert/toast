
# check which input code files are modified in the repo
# usage: call gitchecked(<list_of_inputdatafiles>,<list_of_code_in_git>,<list_of_outputs>) in the input field of a rule
def gitchecked(inputdata,code,outputdata) :
    # the snakemake rule expects a function
    # we are not going to use the wildcards
    def checker(wildcards) :
        updatedCode=[]
        from gitcheck import updatedRepo
        for cfile in code :
           for dfile in outputdata :
             # check if code is younger than outputdata
             if updatedRepo(cfile.format(wildcards=wildcards),dfile.format(wildcards=wildcards)) : updatedCode.append(cfile.format(wildcards=wildcards))
        return updatedCode+[d.format(wildcards=wildcards) for d in inputdata]
    return checker

#######
# set the work directory
#######

# set the working directory
# to make cohexisting the branching ratio and amplitude analysis,
# I'm doing a kind of hack
import os

if ("CI_WORKDIR_AMPL" in os.environ) :
   work_dir = os.environ.get("CI_WORKDIR_AMPL")
else :
   work_dir = "/tmp/TOASTwork_ampl"

os.environ["CI_WORKDIR"] = work_dir

#######
# import the logging configuration
#######

#import logging
#from logging.config import fileConfig

#logging.config.fileConfig('include/logging.ini')

#######
# set the subdirectories that will be used in the analysis
#######

toast_folder = os.getcwd()
os.environ["TOAST_FOLDER"] = toast_folder
  
#######

# main rule, to test the machinery
rule test_toast:
  input:
    work_dir + "/fit_toyMC_MLcD0.pdf"
        
#########

# toy pseudo-experiments
include: "toyMC.snake"

